<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="no"/>

<!-- remove all namespaces from a CDISC-ODM file and also remove some secuTrial metadata information about the status of forms -->
<!-- Version 2 -->
<!-- christian.bauer@med.uni-goettingen.de -->

<xsl:template match="/|comment()|processing-instruction()">
	<xsl:if test="not(contains(./@OID, 'FG.MNP')) or not(contains(./@ItemGroupOID, 'FG.MNP'))">
		<xsl:copy>
		  <xsl:apply-templates/>
		</xsl:copy>
	</xsl:if>
</xsl:template>


<xsl:template match="*">
	<xsl:if test="not(contains(./@OID, 'FG.MNP')) and
		not(contains(./@ItemGroupOID, 'FG.MNP')) and 
		not(contains(./@OID, 'FG.FS')) and
		not(contains(./@ItemGroupOID, 'FG.FS'))		
		">
		<xsl:element name="{local-name()}">
		  <xsl:apply-templates select="@*|node()"/>
		</xsl:element>
	</xsl:if>
</xsl:template>

<xsl:template match="@*">

		<xsl:attribute name="{local-name()}">
		  <xsl:value-of select="normalize-space(.)"/>
		</xsl:attribute>

</xsl:template>

</xsl:stylesheet>